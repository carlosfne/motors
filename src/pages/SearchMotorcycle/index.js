import React, { useState, useMemo } from 'react';

import { GoLocation } from 'react-icons/go';
import { MdChevronRight } from 'react-icons/md';
import DefaultLayout from '~/pages/layout/default';

import SelectInput from '~/components/SelectInput';

import {
  Container,
  Content,
  Section,
  Group,
  InnerGroup,
  AdvancedSearch,
  ActionsContainer,
  CleanButton,
  SubmitButton,
} from './styles';

export default function SearchMotorcycle() {
  const radiusList = useMemo(
    () => [
      { value: 100, label: '100km' },
      { value: 200, label: '200km' },
      { value: 300, label: '300km' },
      { value: 400, label: '400km' },
    ],
    []
  );

  const yearsList = useMemo(
    () => [
      { value: 2022, label: '2022' },
      { value: 2021, label: '2021' },
      { value: 2020, label: '2020' },
    ],
    []
  );

  const pricesList = useMemo(
    () => [
      { value: 20000, label: 'R$ 100.000,00' },
      { value: 40000, label: 'R$ 110.000,00' },
      { value: 60000, label: 'R$ 120.000,00' },
    ],
    []
  );

  const citysList = useMemo(
    () => [
      { value: 2, label: 'Manaus - AM' },
      { value: 3, label: 'Rio de Janeiro - RJ' },
      { value: 1, label: 'São Paulo - SP' },
    ],
    []
  );
  const [radius, setRadius] = useState(radiusList[0]);

  const [state, setState] = useState(citysList);
  return (
    <DefaultLayout>
      <Content>
        <Container>
          <Section>
            <Group>
              <InnerGroup>
                <SelectInput
                  icon={<GoLocation />}
                  label="Onde"
                  placeholder="Cidade / Estado"
                  style={{ flex: 2.5 }}
                  options={citysList}
                  value={state}
                  onChange={setState}
                />
                <SelectInput
                  label="Raio"
                  placeholder=""
                  options={radiusList}
                  value={radius}
                  onChange={setRadius}
                />
              </InnerGroup>

              <InnerGroup>
                <SelectInput
                  className="margin-right"
                  placeholder="Ano Desejado"
                  options={yearsList}
                />
                <SelectInput
                  placeholder="faixa de Preço"
                  options={pricesList}
                />
              </InnerGroup>
            </Group>

            <Group>
              <InnerGroup>
                <SelectInput
                  className="margin-right"
                  isClearable={false}
                  label="Marca"
                  placeholder=""
                  value="TODOS"
                />

                <SelectInput
                  label="Modelo"
                  isClearable={false}
                  value="TODOS"
                  placeholder=""
                />
              </InnerGroup>

              <SelectInput
                label="Versão"
                placeholder=""
                isClearable={false}
                value="TODOS"
              />
            </Group>
          </Section>
          <Section>
            <AdvancedSearch>
              <MdChevronRight />
              Busca Avançada
            </AdvancedSearch>

            <ActionsContainer>
              <CleanButton onClick={() => alert('No function for load Motors')}>
                Limpar filtros
              </CleanButton>
              <SubmitButton
                onClick={() => alert('No function for load Motors')}
              >
                VER OFERTAS
              </SubmitButton>
            </ActionsContainer>
          </Section>
        </Container>
      </Content>
    </DefaultLayout>
  );
}
