import React, { useCallback, useState, useEffect, useMemo } from 'react';
import { GoLocation } from 'react-icons/go';
import { MdChevronRight } from 'react-icons/md';

import api from '~/services/api';

import SelectInput from '~/components/SelectInput';
import VehicleCard from '~/components/VehicleCard';
import DefaultLayout from '~/pages/layout/default';

import {
  Container,
  Content,
  Section,
  Group,
  InnerGroup,
  AdvancedSearch,
  ActionsContainer,
  CleanButton,
  SubmitButton,
  List,
  CheckboxContainer,
  Checkbox,
} from './styles';

export default function SearchCar() {
  const todosObj = useMemo(() => ({ value: -1, label: 'Todos(as)' }), []);

  const lightningList = useMemo(
    () => [
      { value: 100, label: '100km' },
      { value: 200, label: '200km' },
      { value: 300, label: '300km' },
      { value: 400, label: '400km' },
    ],
    []
  );

  const yearsList = useMemo(
    () => [
      { value: 2022, label: '2022' },
      { value: 2021, label: '2021' },
      { value: 2020, label: '2020' },
    ],
    []
  );

  const pricesList = useMemo(
    () => [
      { value: 20000, label: 'R$ 100.000,00' },
      { value: 40000, label: 'R$ 110.000,00' },
      { value: 60000, label: 'R$ 120.000,00' },
    ],
    []
  );

  const citysList = useMemo(
    () => [
      { value: 2, label: 'Manaus - AM' },
      { value: 3, label: 'Rio de Janeiro - RJ' },
      { value: 1, label: 'São Paulo - SP' },
    ],
    []
  );

  const [radius, setRadius] = useState(lightningList[0]);

  const [state, setState] = useState(citysList[0]);

  const [brand, setBrand] = useState(todosObj);
  const [brands, setBrands] = useState([todosObj]);

  const [model, setModel] = useState(todosObj);
  const [models, setModels] = useState([todosObj]);

  const [version, setVersion] = useState(todosObj);
  const [versions, setVersions] = useState([todosObj]);

  const [vehicles, setVehicles] = useState([]);

  const [isNews, setIsNews] = useState(true);
  const [isUsed, setIsUsed] = useState(false);

  useEffect(() => {
    async function loadMakes() {
      const response = await api.get('/OnlineChallenge/Make');

      const Brand = await response.data.map((brand) => ({
        label: brand.Name,
        value: brand.ID,
      }));

      setBrands([todosObj, ...Brand]);
    }
    loadMakes();
  }, [todosObj]);

  /**
   * Load models
   */
  useEffect(() => {
    async function loadModels() {
      const response = await api.get('/OnlineChallenge/Model', {
        params: {
          MakeID: brand.value,
        },
      });
      const Models = response.data.map((model) => ({
        value: model.ID,
        label: model.Name,
        data: model,
      }));

      setModels([todosObj, ...Models]);
    }

    loadModels();
  }, [brand, todosObj]);

  useEffect(() => {
    async function loadVersions() {
      const response = await api.get('/OnlineChallenge/Version', {
        params: {
          ModelID: model.value,
        },
      });

      const Versions = response.data.map((version) => ({
        value: version.ID,
        label: version.Name,
        data: version,
      }));

      setVersions([todosObj, ...Versions]);
    }
    loadVersions();
  }, [model, todosObj]);

  const loadVehicles = useCallback(
    async (page = 1) => {
      const response = await api.get('/OnlineChallenge/Vehicles', {
        params: {
          Page: page,
        },
      });

      if (page === 1) {
        setVehicles(response.data);
      } else {
        setVehicles([...vehicles, response.data]);
      }
    },
    [vehicles]
  );

  useEffect(() => {
    loadVehicles();
  }, []);

  function onBrandChange(value) {
    setBrand(value);
    setModel(models[0]);
    setVersion(versions[0]);
  }

  function onModelChange(value) {
    setModel(value);
    setVersion(versions[0]);
  }

  return (
    <DefaultLayout>
      <Content>
        <Container>
          <CheckboxContainer>
            <Checkbox>
              <input
                type="checkbox"
                id="isNews"
                checked={isNews}
                onChange={() => setIsNews(!isNews)}
              />
              <label htmlFor="isNews">Novos</label>
            </Checkbox>

            <Checkbox>
              <input
                type="checkbox"
                id="isUsed"
                checked={isUsed}
                onChange={() => setIsUsed(!isUsed)}
              />
              <label htmlFor="isUsed">Usados</label>
            </Checkbox>
          </CheckboxContainer>

          <Section>
            <Group>
              <InnerGroup>
                <SelectInput
                  icon={<GoLocation />}
                  label="Onde"
                  placeholder="Cidade / Estado"
                  style={{ flex: 2.5 }}
                  options={citysList}
                  value={state}
                  onChange={setState}
                />
                <SelectInput
                  label="Raio"
                  placeholder=""
                  options={lightningList}
                  value={radius}
                  onChange={setRadius}
                />
              </InnerGroup>

              <InnerGroup>
                <SelectInput
                  className="margin-right"
                  placeholder="Ano Desejado"
                  options={yearsList}
                />
                <SelectInput
                  placeholder="faixa de Preço"
                  options={pricesList}
                />
              </InnerGroup>
            </Group>

            <Group>
              <InnerGroup>
                <SelectInput
                  className="margin-right"
                  isClearable={false}
                  label="Marca"
                  placeholder=""
                  value={brand}
                  onChange={onBrandChange}
                  options={brands}
                />

                <SelectInput
                  label="Modelo"
                  isClearable={false}
                  value={model}
                  placeholder=""
                  options={models}
                  onChange={onModelChange}
                />
              </InnerGroup>

              <SelectInput
                label="Versão"
                placeholder=""
                isClearable={false}
                value={version}
                onChange={setVersion}
                options={versions}
              />
            </Group>
          </Section>
          <Section>
            <AdvancedSearch>
              <MdChevronRight />
              Busca Avançada
            </AdvancedSearch>

            <ActionsContainer>
              <CleanButton>Limpar filtros</CleanButton>
              <SubmitButton onClick={() => loadVehicles(1)}>
                VER OFERTAS
              </SubmitButton>
            </ActionsContainer>
          </Section>
        </Container>
      </Content>

      <List>
        {/* {

        } */}
        {vehicles.map((vehicle) => (
          <VehicleCard key={String(vehicle.ID)} vehicle={vehicle} />
        ))}
      </List>
    </DefaultLayout>
  );
}
