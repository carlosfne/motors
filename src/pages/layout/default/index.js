import React from 'react';
import { FaMotorcycle, FaCar } from 'react-icons/fa';
import { Container, Logo, Header, NavTab } from './styles';
import image from '../../../assets/logo-webmotors.png';

export default function DefaultLayout({ children }) {
  return (
    <Container>
      <Logo src={image} />
      <Header>
        <div>
          <NavTab to="/" exact>
            <FaCar />
            <span>
              <p>COMPRAR</p>
              CARROS
            </span>
          </NavTab>
          <NavTab to="/search-motorcycle">
            <FaMotorcycle />
            <span>
              <p>COMPRAR</p>
              MOTOS
            </span>
          </NavTab>
        </div>

        <button type="button">Vender meu carro</button>
      </Header>
      {children}
    </Container>
  );
}
